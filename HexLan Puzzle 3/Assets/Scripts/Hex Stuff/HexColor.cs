﻿using UnityEngine;

public enum HexColor
{
    RED = 0,
    BLUE = 1,
    YELLOW = 2,
    GREEN = 3,
    WHITE = 4,
    BLACK = 5,
    CLEAR
}


public static class HexColorExt
{
    private static Color[] colors =
    {
        new Color(1f, 0.2078f, 0.3922f),
        Color.cyan,
        new Color(1f, 1f, 0.3137f),
        new Color(0.5f, 1f, 0.5f),
        Color.white,
        Color.black
    };

    public static Color Get(HexColor color)
    {
        if (color < HexColor.CLEAR)
            return colors[(int)color];
        else
            return Color.clear;
    }
}
