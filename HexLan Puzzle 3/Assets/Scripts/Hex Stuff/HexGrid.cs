﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class HexGrid : MonoBehaviour
{
    private Vector3 myOffset;

    public HexCell cellPrefab;
    private HexCell[] cells;

    Canvas gridCanvas;
    public Text cellLabelPrefab;

    private float scrollLimit = -157f;
    private float baseLimit = -19f;

    public bool showCoordinates;

    public HexCell[] Cells
    {
        get
        {
            return cells;
        }

        set
        {
            this.cells = value;
        }
    }

    public float ScrollLimit
    {
        get
        {
            return scrollLimit;
        }

        set
        {
            scrollLimit = value;
        }
    }

    public float BaseLimit
    {
        get
        {
            return baseLimit;
        }

        set
        {
            baseLimit = value;
            Vector3 pos = transform.position;
            pos.y = baseLimit;
            transform.position = pos;
        }
    }

    private Dictionary<Vector3, HexCell> hexMap = new Dictionary<Vector3, HexCell>();

    void Awake()
    {
        myOffset = transform.position;
        baseLimit = myOffset.y;
        gridCanvas = GetComponentInChildren<Canvas>();
        scrollLimit = scrollLimit * this.transform.localScale.x;
    }

    public void CreateGrid(int width, int height)
    {
        cells = new HexCell[height * width];

        for (int z = 0, i = 0; z < height; z++)
        {
            for (int x = 0; x < width; x++)
            {
                CreateCell(x, z, i++);
            }
        }
    }
    
    void CreateCell(int x, int z, int i)
    {
        Vector3 position;
        position.x = x * (HexMetrics.outerRadius * 1.43f);
        position.y = (z - x * 0.5f + x / 2) * (HexMetrics.innerRadius * 1.9f);
        position.z = 1f * (1f + (i / 7));

        HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
        cell.transform.SetParent(transform, false);
        cell.transform.localPosition = position;
        cell.Coordinates = HexCoordinates.FromOffsetCoordinates(x, z);

        Vector3 coords = cell.Coordinates.HexCoords;
        hexMap.Add(coords, cell);
        //HexManager.Instance.AddToHexDic(coordinates, cell);
        //if (showCoordinates)
        //{
        //    Text label = Instantiate<Text>(cellLabelPrefab);
        //    label.rectTransform.SetParent(gridCanvas.transform, false);
        //    label.rectTransform.anchoredPosition = new Vector2(position.x, position.y);
        //    label.text = cell.coordinates.ToString();
        //}
    }

    //public void ScrollGrid(Vector3 offset)
    //{
    //    if (PlayerController.UnitSelected)
    //        return;

    //    Vector3 pos = transform.position;
    //    pos.y += offset.y;
    //    if (pos.y < scrollLimit)
    //    {
    //        pos.y = scrollLimit;
    //    }
    //    else if (pos.y > baseLimit)
    //    {
    //        pos.y = baseLimit;
    //    }
    //    transform.position = pos;
    //}

    public void AdjustPosition(float factor)
    {
        Vector3 pos = new Vector3(factor * 28f, 0f, 0f);
        transform.localPosition -= pos;
    }

    public void SetZoom(float factor)
    {
        Vector3 scale = new Vector3(factor, factor, factor);
        transform.localScale = scale;
    }

    public void InitialiseTransform()
    {
        transform.position = myOffset;
        transform.localScale = Vector3.one;
    }

    public HexCell GetAdyacent(HexCell cell, HexDirection direction)
    {
        Vector3 coords = cell.Coordinates.HexCoords;
        switch (direction)
        {
            case HexDirection.UP:
                coords += new Vector3(0, -1, 1);
                break;
            case HexDirection.UPRIGHT:
                coords += new Vector3(1, -1, 0);
                break;
            case HexDirection.DOWNRIGHT:
                coords += new Vector3(1, 0, -1);
                break;
            case HexDirection.DOWN:
                coords += new Vector3(0, 1, -1);
                break;
            case HexDirection.DOWNLEFT:
                coords += new Vector3(-1, 1, 0);
                break;
            case HexDirection.UPLEFT:
                coords += new Vector3(-1, 0, 1);
                break;
            default:
                break;
        }
        HexCell ret = null;
        if (hexMap.TryGetValue(coords, out ret))
        {
            return ret;
        }
        else
        {
            return null;
        }
    }

}
