﻿
public enum HexDirection
{
    UP = 0,
    UPRIGHT = 1,
    DOWNRIGHT = 2,
    DOWN = 3,
    DOWNLEFT = 4,
    UPLEFT = 5,

    COUNT
}
