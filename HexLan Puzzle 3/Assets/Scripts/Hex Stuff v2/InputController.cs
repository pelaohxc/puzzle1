﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

    private bool draggingItem = false;
    private GameObject draggedObject;
    private Vector2 touchOffset;
    public float factorEscala;

    void Update()
    {
        if (HasInput)
        {
            DragOrPickUp();
        }
        else
        {
            if (draggingItem)
                DropItem();
        }

        this.gameObject.transform.position = new Vector3(-1.5f, 2f, 0f);

    }

    Vector2 CurrentTouchPosition
    {
        get
        {
            Vector2 inputPos;
            inputPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            return inputPos;
        }
    }

    private void DragOrPickUp()
    {
        var inputPosition = CurrentTouchPosition;

        if (draggingItem)
        {
            draggedObject.transform.position = inputPosition + touchOffset;
        }
        else
        {
            RaycastHit2D[] touches = Physics2D.RaycastAll(inputPosition, inputPosition, Mathf.Infinity, ~(1 << 8));

            if (touches.Length > 0)
            {
                var hit = touches[0];
                if (hit.transform != null)
                {
                    if (hit.transform.gameObject.tag.Equals("Draggable"))
                    {
                        
                        draggedObject = hit.transform.gameObject;
                        if (draggedObject.GetComponent<ColliderDetection>().puedeMover)
                        {
                            draggingItem = true;
                            touchOffset = (Vector2)hit.transform.position - inputPosition;
                            if (col)
                            {
                                draggedObject.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
                            }
                            else
                            {

                                draggedObject.transform.localScale = new Vector3(draggedObject.transform.localScale.x * factorEscala, draggedObject.transform.localScale.y * factorEscala, draggedObject.transform.localScale.z * factorEscala);

                            }
                        }
                    }
                    else
                    {
                        Debug.Log("No toco nada");
                        return;
                    }
                }
                else
                {
                    Debug.Log("No toco nada");
                }
            }
        }
    }

    private bool HasInput
    {
        get
        {
            // returns true if either the mouse button is down or at least one touch is felt on the screen
            return Input.GetMouseButton(0);
        }
    }

    void DropItem()
    {
        draggingItem = false;
        if (!col)
        {
            draggedObject.transform.localScale = new Vector3(1f, 1f, 1f);
            col = false;
        }else
        {
            draggedObject.transform.localScale = new Vector3(1f, 1f, 1f);
            col = false;
        }
        if (draggedObject.GetComponent<ColliderDetection>().isInRightPlace)
        {
            draggedObject.GetComponent<ColliderDetection>().puedeMover = false;
            draggedObject.GetComponent<ColliderDetection>().ActualizarPosicion();
        }
        else
        {
            draggedObject.GetComponent<ColliderDetection>().puedeMover = true;
        }
    }

    //Detectando Colisiones

    public bool col;

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    col = true;
    //    for (int i = 0; i < collision.gameObject.transform.childCount; i++)
    //    {
    //        collision.gameObject.transform.GetChild(i).transform.GetChild(0).gameObject.SetActive(true);
    //        collision.gameObject.transform.GetChild(i).transform.GetChild(1).gameObject.SetActive(false);
    //    }
    //}

    private void OnTriggerExit2D(Collider2D collision)
    {
        col = false;
        draggedObject.transform.localScale = new Vector3(1f, 1f, 1f);
        collision.gameObject.transform.localScale.Set(0.5f, 0.5f, 1f);
        
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        col = true;
        
    }


}
