﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour {

    public int level;
    public bool isLevelSelected = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isLevelSelected)
        {
            CargarNivel(level);
        }
	}

    void CargarNivel(int nivel)
    {
        SceneManager.LoadScene(nivel);
    }
}
