﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderDetection : MonoBehaviour
{

    public bool puedeMover = true;
    public bool isInRightPlace = false;
    public Vector3 posicion;
    private bool puedeCambiar = true;
    private int cantidadPiezas;
    // Use this for initialization
    void Start()
    {
        cantidadPiezas = GameObject.Find("Grid").GetComponent<GridGeneration>().numeroDePiezas;   
        posicion = this.transform.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        for(int i = 0; i < cantidadPiezas; i++)
        {
            if (collision.gameObject.name == "PiezaTablero"+i && this.gameObject.name == "Pieza"+i)
            {
                for (int j = 0; j < this.gameObject.transform.childCount; j++)
                {
                    if (puedeCambiar)
                    {
                        this.gameObject.transform.GetChild(j).transform.GetChild(0).gameObject.SetActive(true);
                        this.gameObject.transform.GetChild(j).transform.GetChild(1).gameObject.SetActive(false);
                    }
                    isInRightPlace = true;
                    posicion = collision.gameObject.transform.position;
                    posicion.y = posicion.y * 3.25f;
                    posicion.z = 0f;
                }
            }
            if (collision.gameObject.name == "PiezaTablero1" && this.gameObject.name == "Pieza1")
            {
                for (int j  = 0; j < this.gameObject.transform.childCount; j++)
                {
                    if (puedeCambiar)
                    {
                        this.gameObject.transform.GetChild(j).transform.GetChild(0).gameObject.SetActive(true);
                        this.gameObject.transform.GetChild(j).transform.GetChild(1).gameObject.SetActive(false);
                    }
                    isInRightPlace = true;
                    posicion = collision.gameObject.transform.position;

                    posicion = collision.gameObject.transform.position;
                    posicion.y = posicion.y * 4.80f;
                    posicion.z = 0f;
                }
            }
        }
        
    }

    public void ActualizarPosicion()
    {
        if (isInRightPlace)
        {
            transform.position = posicion;
            puedeCambiar = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        for (int i = 0; i < this.gameObject.transform.childCount; i++)
        {
            if (puedeCambiar)
            {
                this.gameObject.transform.GetChild(i).transform.GetChild(0).gameObject.SetActive(false);
                this.gameObject.transform.GetChild(i).transform.GetChild(1).gameObject.SetActive(true);
            }

            isInRightPlace = false;
        }

    }
}
