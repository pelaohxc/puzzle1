﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGeneration : MonoBehaviour {

    public HexGrid grid;
    public HexGrid piezasGrid;

    public GameObject[] piezas;
    public GameObject[] piezasTablero;

    public int ancho;
    public int alto;
    public int numeroDePiezas;

    public float factorEscala;

    void Awake()
    {
    }

    // Use this for initialization
    void Start() {
        grid.CreateGrid(ancho, alto);

        piezasGrid.CreateGrid(ancho, alto);
        AsignarTag(grid, "Undefined");

        if (ancho == 3 && alto == 2)
        {
            grid.transform.position = new Vector3(-1.5f, -0.4f);
        }
        else
        {
            grid.transform.position = Camera.main.transform.position;
        }
        DividirGrid(numeroDePiezas);
        DividirGridPanel(numeroDePiezas);
        
    }

    private void DividirGridPanel(int numeroDePiezas)
    {
        piezasTablero = new GameObject[numeroDePiezas];
        AsignarTag(grid, "Tablero");
        GenerarPiezasTablero(piezasTablero, grid, numeroDePiezas);
        DividirEnPiezasTablero(numeroDePiezas, grid, piezasTablero, grid.Cells.Length);
        crearCollisionadorGrid(piezasTablero, numeroDePiezas);
        reasignarParent(piezasTablero);
        reasignarParent(piezasTablero);
    }

    private void reasignarParent(GameObject[] piezasTablero)
    {
        for (int i = 0; i < piezasTablero.Length;i++)
        {
            for (int j = 0; j < piezasTablero[i].gameObject.transform.childCount; j++)
            {
                piezas[i].gameObject.transform.GetChild(j).gameObject.transform.parent = GameObject.Find("Grid").transform;
            }
        }
    }

    private void GenerarPiezasTablero(GameObject[] piezasTablero, HexGrid grid, int numeroDePiezas)
    {
        for (int i = 0; i < numeroDePiezas; i++)
        {
            piezas[i] = new GameObject("PiezaTablero" + i);
            piezas[i].transform.position = grid.Cells[i].transform.position;
            piezas[i].transform.parent = this.gameObject.transform;
            piezasTablero[i] = piezas[i];

        }
    }

    private void DividirEnPiezasTablero(int numeroDePiezas, HexGrid Grid, GameObject[] pArr, int length)
    {
        int factor = 0;
        int j = 0;
        float temp = length / numeroDePiezas;
        if (length % numeroDePiezas == 0)
        {
            for (int i = 0; i < numeroDePiezas; i++)
            {
                if (j != length)
                {
                    factor = 0;
                    do
                    {
                        Grid.Cells[j].transform.parent = pArr[i].transform;
                        j++;
                        factor++;
                    } while (temp > factor);
                }
                
            }
        }
    }

    private void AsignarTag(HexGrid grid, String t)
    {
        for (int i = 0; i < grid.Cells.Length; i++)
        {
            grid.Cells[i].tag = t;

            //solo como prueba
            grid.Cells[i].GetComponent<PolygonCollider2D>().enabled = false;
            grid.Cells[i].transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    
    void DividirGrid(int cantidadPiezas)
    {
        piezas = new GameObject[cantidadPiezas];

        AsignarTag(piezasGrid, "Undefined");
        GenerarPiezas(piezas, piezasGrid, cantidadPiezas);
        DividirEnPiezas(cantidadPiezas, piezasGrid, piezas, piezasGrid.Cells.Length);

        AsignarTag(piezas, "Draggable", cantidadPiezas);
        crearCollisionador(piezas, cantidadPiezas);
        

        
    }

    private void DividirEnPiezas(int cantidadPiezas, HexGrid piezasGrid, GameObject[] piezas, int numeroDeCeldas)
    {
        int factor = 0;
        int j = 0;
        float temp = numeroDeCeldas / cantidadPiezas;
        if(numeroDeCeldas % cantidadPiezas == 0)
        {
            for (int i = 0; i < cantidadPiezas; i++)
            {
                if (j != numeroDeCeldas)
                {
                    factor = 0;
                    do
                    {
                        piezasGrid.Cells[j].transform.parent = piezas[i].transform;
                        
                        j++;
                        factor++;
                    } while (temp > factor);
                }
            }
        }
    }

    private void GenerarPiezas(GameObject[] piezas, HexGrid piezasGrid, int cantidadPiezas)
    {
        for (int i = 0; i < cantidadPiezas; i++)
        {
            piezas[i] = new GameObject("Pieza" + i);
            piezas[i].transform.position = grid.Cells[i].transform.position;
            piezas[i].gameObject.transform.position.Set(piezas[i].transform.position.x, piezas[i].transform.position.y +2, 0f);


        }
    }

    

    private void crearCollisionador(GameObject[] piezas, int cantidadPiezas)
    {

        for (int i = 0; i < cantidadPiezas; i++)
        {
            for (int j = 0; j < piezas[i].transform.childCount; j++)
            {
                piezas[i].AddComponent<PolygonCollider2D>();
                piezas[i].GetComponents<PolygonCollider2D>()[j].points = piezas[i].transform.GetChild(j).GetComponent<PolygonCollider2D>().points;
                piezas[i].GetComponents<PolygonCollider2D>()[j].offset = piezas[i].transform.GetChild(j).transform.localPosition;
                deactivateChildColliders(piezas[i].transform.GetChild(j).gameObject);
                piezas[i].GetComponents<PolygonCollider2D>()[j].isTrigger = true;
            }

            //Agrega un rigidbody sin gravedad y con rotacion estatica
            piezas[i].AddComponent<Rigidbody2D>();
            piezas[i].GetComponent<Rigidbody2D>().gravityScale = 0;
            piezas[i].GetComponent<Rigidbody2D>().freezeRotation = true;
            piezas[i].AddComponent<ColliderDetection>();
            piezas[i].AddComponent<PiezasTransform>();

        }
    }

    private void crearCollisionadorGrid(GameObject[] piezas, int cantidadPiezas)
    {
        for (int i = 0; i < cantidadPiezas; i++)
        {
            for (int j = 0; j < piezas[i].transform.childCount; j++)
            {
                piezas[i].gameObject.AddComponent<PolygonCollider2D>();
                piezas[i].gameObject.GetComponents<PolygonCollider2D>()[j].points = piezas[i].transform.GetChild(j).GetComponent<PolygonCollider2D>().points;
                piezas[i].gameObject.GetComponents<PolygonCollider2D>()[j].offset = piezas[i].transform.GetChild(j).transform.localPosition;
                deactivateChildColliders(piezas[i].transform.GetChild(j).gameObject);
                piezas[i].gameObject.GetComponents<PolygonCollider2D>()[j].isTrigger = true;
                
            }

        }
    }
    private void AsignarTag(GameObject[] piezas, string v, int cantidadPiezas)
    {
        for (int i = 0; i < cantidadPiezas; i++)
        {
            piezas[i].tag = v;
        }
    }

    private void deactivateChildColliders(GameObject gameObject)
    {
        gameObject.GetComponent<PolygonCollider2D>().enabled = false;
    }
}
