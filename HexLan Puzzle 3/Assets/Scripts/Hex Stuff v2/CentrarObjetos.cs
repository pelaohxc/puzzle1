﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentrarObjetos : MonoBehaviour {
    private Vector3 originalPosition;
    private Vector3 centeredPosition;


	// Use this for initialization
	void Start () {
        centeredPosition = Camera.main.transform.position;
        originalPosition = this.gameObject.transform.position;
        this.gameObject.transform.position = centeredPosition;
	}
	
	// Update is called once per frame
	void Update () {
        this.gameObject.transform.position = centeredPosition;
    }
}
