﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateGrid : MonoBehaviour {

    public GameObject HexCell;
    public int ancho, alto;
    public GameObject[,] grid;
	// Use this for initialization
	void Start () {
        //crearGrid();
        instanciarGrid();
	}

    private void instanciarGrid()
    {
        
        for (int i = 0; i < ancho; i++)
        {
            for (int j = 0; j < alto; j++)
            {
                Instantiate(HexCell, new Vector3(i*2, j*2, 0), Quaternion.identity);
            }
            
        }
    }

    // Update is called once per frame
    void Update () {
       
    }

    void crearGrid()
    {
        grid = new GameObject[ancho, alto];
        for (int i = 0; i < grid.GetLength(0); i++)
        {
            for (int j = 0; j < grid.GetLength(1); j++)
            {
                grid[i, j] = HexCell.gameObject;
            }
        }
        
    }
}
