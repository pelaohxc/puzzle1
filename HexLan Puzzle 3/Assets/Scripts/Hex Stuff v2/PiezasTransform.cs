﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiezasTransform : MonoBehaviour {
    private Vector3 pos;
	// Use this for initialization
	void Start () {
        pos = new Vector3(this.transform.position.x * 2f, this.transform.position.y, this.transform.position.z);
        this.transform.position = pos;
    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
