﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexCell : MonoBehaviour
{
    [SerializeField]
    private GameObject center;
    public GameObject selected;
    private HexCoordinates coordinates;
    public HexCoordinates Coordinates
    {
        get
        {
            return coordinates;
        }

        set
        {
            coordinates = value;
        }
    }

    public bool Active
    {
        get
        {
            return center.activeInHierarchy;
        }
    }

    public void SetActive(bool value)
    {
        center.SetActive(value);
    }

    // Use this for initialization
    void Start()
    {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
